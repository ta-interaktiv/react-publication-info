<a name="1.0.0"></a>
# 1.0.0 (2017-07-28)


### Chore

* IDE changes ([55192bea21a6eeb278636d39d66bcc2f82419a89](https://gitlab.com/ta-interactive/react-publication-info/commit/55192bea21a6eeb278636d39d66bcc2f82419a89))
* Update package.json data and Readme ([acd653f03c401c307c34ffa98d64159783659771](https://gitlab.com/ta-interactive/react-publication-info/commit/acd653f03c401c307c34ffa98d64159783659771))

### Feature

* Added generic NameValueItem component. ([5932de52df608f8653fa09227dc397910f3a7c53](https://gitlab.com/ta-interactive/react-publication-info/commit/5932de52df608f8653fa09227dc397910f3a7c53))
* Added TimestampFormatter component. ([ea3a7920f90c36029d903cb744cdb1814355f4fe](https://gitlab.com/ta-interactive/react-publication-info/commit/ea3a7920f90c36029d903cb744cdb1814355f4fe))
* Added VersionFormatter component. ([61310d393d891277bce436a6fe5993507bdd2ee2](https://gitlab.com/ta-interactive/react-publication-info/commit/61310d393d891277bce436a6fe5993507bdd2ee2))

### Refactoring

* Changed TimestampFormatter to use the generic component ([90f913d0b224731a71e89d4e57e6dfaa9c9e40d2](https://gitlab.com/ta-interactive/react-publication-info/commit/90f913d0b224731a71e89d4e57e6dfaa9c9e40d2))
* Improved PublicationInfo ([152b2fce96fdb94f1e25ce6f44c1e001af126faa](https://gitlab.com/ta-interactive/react-publication-info/commit/152b2fce96fdb94f1e25ce6f44c1e001af126faa))
