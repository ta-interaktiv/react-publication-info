<a name="1.4.0"></a>
# [1.4.0](https://gitlab.com/ta-interactive/react-component-setup/compare/1.3.0...1.4.0) (2017-07-25)


### chore

* Added flow as a dependency. ([a79f84261160caad24013841b7aca12e027a2251](https://gitlab.com/ta-interactive/react-component-setup/commit/a79f84261160caad24013841b7aca12e027a2251))
* Changed document generation to documentation.js ([d3c57b08a24afb301eb1616c85ef9a3430ff5a01](https://gitlab.com/ta-interactive/react-component-setup/commit/d3c57b08a24afb301eb1616c85ef9a3430ff5a01))
* Updated dependencies ([2e760058a46740cae7e6908c07a4656531ed57d7](https://gitlab.com/ta-interactive/react-component-setup/commit/2e760058a46740cae7e6908c07a4656531ed57d7))

### test

* Added Jest testing framework. ([2d30ddcd993c75286611d6a57518a5f63348d5d6](https://gitlab.com/ta-interactive/react-component-setup/commit/2d30ddcd993c75286611d6a57518a5f63348d5d6))



<a name="1.0.0"></a>
# 1.0.0 (2016-10-31)




