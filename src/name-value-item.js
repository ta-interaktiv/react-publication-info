// @flow

import React from 'react'

type Props = {
  name?: string,
  value?: string,
}

export type ClassNames = {
  itemClassName?: string,
  nameClassName?: string,
  valueClassName?: string
}

/**
 * Generic key-value component.
 *
 * @example
 * import { NameValueItem }from '@ta-interaktiv/react-publication-info'
 *
 * function Component(props) {
 *   return(
 *     <NameValueItem
 *       name='Version'
 *       value='1.0.0'
 *       itemClassName='ui label'
 *       valueClassName='detail'
 *     />
 *   )
 * }
 *
 * // returns <span className="name-value-item ui label">
 * //           <span className="name ">Version</span>
 * //           <span className="value detail">1.0.0</span>
 * //         </span>
 *
 *
 * @param {object} props
 * @param {string} [props.name] The name or key of the information.
 * @param {string} [props.value] The value. If null or undefined, the component will not render.
 * @param {string} [props.itemClassName] Class name(s) for the whole item.
 * @param {string} [props.nameClassName] Class names(s) for the key  part.
 * @param {string} [props.valueClassName] Class names(s) for the value part.
 */
export function NameValueItem ({
  name, value,
  itemClassName = '',
  nameClassName = '',
  valueClassName = ''
  }: Props & ClassNames): React.Element<any> | null {
  if (!value) {
    return null
  }

  return <span className={`name-value-item ${itemClassName}`}>
    {name && name !== '' &&
    <span className={`name ${nameClassName}`}>{name}&nbsp;</span>
    }
    <span className={`value ${valueClassName}`}>{value}</span>
  </span>
}

export default NameValueItem
