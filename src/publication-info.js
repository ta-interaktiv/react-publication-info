// @flow
import React from 'react'
import { TimestampFormatter, VersionFormatter } from './index'
import type { ClassNames } from './name-value-item'

type Props = { publicationDate?: number, changeDate?: number, version?: string, className?: string }

/**
 * Show all information regarding a publication: when it has been first published, when it was updated, and what version it is.
 *
 * @param {object} props
 * @param {string} [props.publicationDate=window.project.publicationDate] Unix timestamp when the project has been first published
 * @param {string} [props.changeDate=window.project.changeDate] Unix timestamp when the project has been last updated.
 * @param {string} [props.version=window.project.version] Version number of the project
 * @param {string} [props.className] CSS class name of the whole object.
 * @param {string} [props.itemClassName] CSS class names for the individual items.
 * @param {string} [props.nameClassName] CSS class names for the key/name of the individual items.
 * @param {string} [props.valueClassName] CSS class names for the value of the individual items.
 */
export function PublicationInfo ({publicationDate, changeDate, version, className = '', itemClassName = '', nameClassName = '', valueClassName = ''}: Props & ClassNames): React.Element<any> {
  if (window.project) {
    publicationDate = publicationDate || window.project.publicationDate
    changeDate = changeDate || window.project.changeDate
    version = version || window.project.version
  }

  // Collect all class names for individual items in a object
  let itemClasses: ClassNames = {itemClassName, nameClassName, valueClassName}

  return <div className={`publication-info ${className}`}>
    <TimestampFormatter prefix='Erstellt: ' timestamp={publicationDate} {...itemClasses} />
    <TimestampFormatter prefix='Letzte Änderung: ' timestamp={changeDate} {...itemClasses} />
    <VersionFormatter version={version} {...itemClasses} />
  </div>
}

export default PublicationInfo
