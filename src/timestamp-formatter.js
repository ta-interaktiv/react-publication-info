// @flow

import React from 'react'
import swissLocaleDefinition from 'de-ch-locale'
import { timeFormatLocale } from 'd3-time-format'
import { NameValueItem } from './index'
import type { ClassNames } from './name-value-item'

type Props = {
  timestamp?: number,
  prefix?: string,
}

/**
 * Swiss German locale – use this to create new formats.
 * @private
 */
const _swissLocale = timeFormatLocale(swissLocaleDefinition)
const fullDateAndTimeFormatter = _swissLocale.format('%x, %H:%M Uhr')

/**
 * Provides a formatted timestamp, with an optional prefix. Modeled after the common article suffix on tagesanzeiger.ch
 * articles, it will return something like "Erstellt: 25.07.2017, 11:47 Uhr" with the appropriate prefix.
 *
 * @example
 * import { TimestampFormatter } from '@ta-interaktiv/react-publication-date'
 *
 * function Component (props) {
 *   return (
 *     <TimestampFormatter prefix='Erstellt:' timestamp={1500986249} />
 *   )
 * }
 *
 * @param {object} props
 * @param {number} [props.timestamp] A UNIX timestamp, in seconds. If not provided, the element will not render.
 * @param {string} [props.prefix] A string that precedes the date, like "Erstellt:" or "Aktualisiert:"
 *
 * @see {@link NameValueItem} for additional className parameters to be passed into this component.
 */
export function TimestampFormatter (props: Props & ClassNames): React.Element<any> | null {
  let {timestamp, prefix = ''} = props
  if (timestamp) {
    return <NameValueItem {...props} name={prefix} value={fullDateAndTimeFormatter(timestamp * 1000)} />
  } else {
    return null
  }
}

export default TimestampFormatter
