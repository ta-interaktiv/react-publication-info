// @flow

import React from 'react'
import type { ClassNames } from './name-value-item'
import { NameValueItem } from './index'

type Props = {
  version?: string,
  prefix?: string
}

/**
 * Provides a formatted version, with an optional prefix.
 *
 * @example
 * import { VersionFormatter } from '@ta-interaktiv/react-publication-date'
 *
 * function Component (props) {
 *   return (
 *     <VersionFormatter version='1.0.0' />
 *   )
 * }
 *
 * @param {object} props
 * @param {number} props.version A semantic version string. If not provided, the element will not render.
 * @param {string} [props.prefix=Version] A string that precedes the version.
 *
 * @see {@link NameValueItem} for additional `className` props that can be passed in.
 */
export function VersionFormatter (props: Props & ClassNames): React.Element<any> | null {
  let {version, prefix = 'Version'} = props
  if (version) {
    return <NameValueItem {...props} name={prefix} value={version} />
  } else {
    return null
  }
}

export default VersionFormatter
