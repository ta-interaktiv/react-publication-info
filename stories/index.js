// @flow
import React from 'react'
import { storiesOf } from '@kadira/storybook'
import backgrounds from 'react-storybook-addon-backgrounds'
import { PublicationInfo } from '../src/index'
import 'ta-semantic-ui/semantic/dist/components/reset.css'
import 'ta-semantic-ui/semantic/dist/components/site.css'
import 'ta-semantic-ui/semantic/dist/components/container.css'
import 'ta-semantic-ui/semantic/dist/components/list.css'
import 'ta-semantic-ui/semantic/dist/components/label.css'
import 'ta-semantic-ui/semantic/dist/components/header.css'
import 'ta-semantic-ui/semantic/dist/components/form.css'

storiesOf('Publication Date', module)
  .addDecorator(backgrounds([
    {name: 'white', value: '#FFFFFF', default: true},
    {name: 'dark', value: '#060606'}
  ]))
  .add('All manual information, as a list', () => (
    <PublicationInfo version='1.1.2' publicationDate={1500986249} changeDate={1500996618}
      className='ui small list' itemClassName='item' nameClassName='header' />
  ))
  .add('All manual information, as labels', () => (
    <PublicationInfo version='1.1.2'
      publicationDate={1500986249}
      changeDate={1500996618}
      className='ui labels'
      itemClassName='ui label'
      valueClassName='detail' />
  ))
  .add('Publication data within a text container', () => (
    <div className='ui text container'>
      <PublicationInfo publicationDate={1500986249}
        className='ui form'
        itemClassName='field'
        itemHeaderClassName='label' />
    </div>
  ))
