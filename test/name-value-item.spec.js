// @flow

import React from 'react'
import renderer from 'react-test-renderer'
import { NameValueItem } from '../src/index'

describe('NameValueItem', () => {
  test('Do not render anything if value is undefined or empty', () => {
    const renderedComponent = renderer.create(
      <NameValueItem />
    )
    const tree = renderedComponent.toJSON()
    expect(tree).toBeNull()
  })

  test('Standard case – no special classes', () => {
    const renderedComponent = renderer.create(
      <NameValueItem name='foo' value='bar' />
    )
    const tree = renderedComponent.toJSON()
    expect(tree).toMatchSnapshot()
  })

  test('Component with additional classes added', () => {
    const renderedComponent = renderer.create(
      <NameValueItem name='foo' value='bar' itemClassName='item' nameClassName='header' valueClassName='content' />
    )
    expect(renderedComponent.toJSON()).toMatchSnapshot()
  })

  test('Component as label', () => {
    const renderedComponent = renderer.create(
      <NameValueItem
        name='Version'
        value='1.0.0'
        itemClassName='ui label'
        valueClassName='detail'
      />
    )
    expect(renderedComponent.toJSON()).toMatchSnapshot()
  })
})
