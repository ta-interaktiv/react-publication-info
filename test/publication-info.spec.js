// @flow
import React from 'react'
import renderer from 'react-test-renderer'
import { PublicationInfo } from '../src/index'

describe('Publication Info', () => {

  afterEach(() => {
    window.project = undefined
  })

  test('Default Setting, without any input', () => {
    const renderedComponent = renderer.create(
      <PublicationInfo />
    )

    const tree = renderedComponent.toJSON()
    expect(tree).toMatchSnapshot()
  })

  test('Just having the publication date', () => {
    const renderedComponent = renderer.create(
      <PublicationInfo publicationDate={1500986249} />
    )

    const tree = renderedComponent.toJSON()
    expect(tree).toMatchSnapshot()
  })

  test('Publication and change date,', () => {
    const renderedComponent = renderer.create(
      <PublicationInfo publicationDate={1500986249} changeDate={1500996618} />
    )

    const tree = renderedComponent.toJSON()
    expect(tree).toMatchSnapshot()
  })

  test('Add information, manually.', () => {
    const renderedComponent = renderer.create(
      <PublicationInfo version='1.1.2' publicationDate={1500986249} changeDate={1500996618} />
    )

    const tree = renderedComponent.toJSON()
    expect(tree).toMatchSnapshot()
  })

  test('Publication date provided by window.project', () => {
    window.project = {
      publicationDate: 1500986249
    }

    const renderedComponent = renderer.create(
      <PublicationInfo />
    )

    const tree = renderedComponent.toJSON()
    expect(tree).toMatchSnapshot()
  })

  test('All data provided by window.project', () => {
    window.project = {
      publicationDate: 1500986249,
      changeDate: 1500996618,
      version: '1.2.0'
    }

    const renderedComponent = renderer.create(
      <PublicationInfo />
    )

    const tree = renderedComponent.toJSON()
    expect(tree).toMatchSnapshot()
  })
})
