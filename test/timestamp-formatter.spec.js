// @flow
import React from 'react'
import renderer from 'react-test-renderer'
import { TimestampFormatter } from '../src/index'

describe('Timestamp Formatter', () => {
  test('Do not render anything if no timestamp is provided', () => {
    const component = renderer.create(
      <TimestampFormatter />
    )
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })

  test('Render the full date with a timestamp', () => {
    const component = renderer.create(
      <TimestampFormatter timestamp={1500986249} />
    )
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })

  test('Render with a Prefix', () => {
    const component = renderer.create(
      <TimestampFormatter timestamp={1500986249} prefix='Erstellt:' />
    )
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })
})
