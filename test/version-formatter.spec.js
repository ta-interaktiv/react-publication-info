// @flow
import React from 'react'
import renderer from 'react-test-renderer'
import { VersionFormatter } from '../src/index'

describe('Version Formatter', () => {
  test('Do not render anything if no version is provided', () => {
    const component = renderer.create(
      <VersionFormatter />
    )
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })

  test('Render the Version number', () => {
    const component = renderer.create(
      <VersionFormatter version='1.1.2' />
    )
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })

  test('Render with a Prefix', () => {
    const component = renderer.create(
      <VersionFormatter version='1.1.2' prefix='Guru Meditation:' />
    )
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })

  test('Render with class names', () => {
    const component = renderer.create(
      <VersionFormatter version='2.0' itemClassName='version ui label' valueClassName='detail' />
    )

    expect(component.toJSON()).toMatchSnapshot()
  })
})
